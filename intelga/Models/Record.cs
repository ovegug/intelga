﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace intelga.Models
{
    public class Record
    {
        public int Id { get; set; }
        public string Temperature { get; set; }
        public double Space { get; set; }
        public DateTime DateTime { get; set; }
        public string UserAgent { get; set; }
        public string UserHostAddress { get; set; }

        public int? CollectorId { get; set; }
        [JsonIgnore]
        public virtual Collector Collector { get; set; }
    }
}