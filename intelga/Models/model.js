﻿function Record(data) {
    this.id = data.Id;
    this.temperature = data.Temperature;
    this.space = data.Space;

    date = new Date(data.DateTime);
    this.dateTime = date.toLocaleTimeString() + " " + date.toLocaleDateString();

    this.userAgent = data.UserAgent;
    this.userHostAddress = data.UserHostAddress;
}

function ApplicationModel() {
    var self = this;
    self.records = ko.observableArray([]);    

    self.loadData = function() {
        $.getJSON(
            "api/Records",
            function (data) {
                var mappedRecords = $.map(data, function (item) { return new Record(item); });
                self.records(mappedRecords);
            });
    };

    self.loadData();

    setInterval(self.loadData, 10000);

    self.clearData = function () {
        $.ajax({ url: "api/ClearRecords" });
        self.records([]);
    }
}

ko.applyBindings(new ApplicationModel());