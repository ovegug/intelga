namespace intelga.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Collectors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Records", "DateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Records", "UserAgent", c => c.String());
            AddColumn("dbo.Records", "UserHostAddress", c => c.String());
            AddColumn("dbo.Records", "CollectorId", c => c.Int());
            CreateIndex("dbo.Records", "CollectorId");
            AddForeignKey("dbo.Records", "CollectorId", "dbo.Collectors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Records", "CollectorId", "dbo.Collectors");
            DropIndex("dbo.Records", new[] { "CollectorId" });
            DropColumn("dbo.Records", "CollectorId");
            DropColumn("dbo.Records", "UserHostAddress");
            DropColumn("dbo.Records", "UserAgent");
            DropColumn("dbo.Records", "DateTime");
            DropTable("dbo.Collectors");
        }
    }
}
