using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using intelga.Models;

namespace intelga.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<intelga.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(intelga.Models.ApplicationDbContext context)
        {
            context.Collectors.Add(new Collector() { Id = 1 });
        }
    }
}
